<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InitDb extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();

        // Guestbook
        Schema::create('guestbook', function (Blueprint $table) {
            // Fields
            $table->increments('id');
            $table->integer('created_by_id')->unsigned();
            $table->integer('deleted_by_id')->unsigned()->nullable();
            $table->string('name')->unique();
            $table->string('description');
            // Timestamps
            $table->timestamps();
            //SoftDeletes
            $table->softDeletesTz();
            // FK
            $table->foreign('created_by_id')->references('id')->on('users')->onDelete('no action')->onUpdate('no action');
            $table->foreign('deleted_by_id')->references('id')->on('users')->onDelete('no action')->onUpdate('no action');
            // Indexing
            $table->index(['id', 'name']);
            $table->index(['created_by_id']);
            $table->index(['deleted_at']);
        });

        // Message
        Schema::create('message', function (Blueprint $table) {
            // Fields
            $table->increments('id');
            $table->integer('created_by_id')->unsigned();
            $table->integer('deleted_by_id')->unsigned()->nullable();
            $table->string('description');
            // Timestamps
            $table->timestamps();
            //SoftDeletes
            $table->softDeletesTz();
            // FK
            $table->foreign('created_by_id')->references('id')->on('users')->onDelete('no action')->onUpdate('no action');
            $table->foreign('deleted_by_id')->references('id')->on('users')->onDelete('no action')->onUpdate('no action');
            // Indexing
            $table->index(['id']);
            $table->index(['created_by_id']);
            $table->index(['deleted_at']);
        });

        // Message History (contains message descriptions before edits were made)
        Schema::create('message_history', function (Blueprint $table) {
            // Fields
            $table->increments('id');
            $table->integer('message_id')->unsigned();
            $table->string('description');
            // Timestamps
            $table->timestamps();
            // FK
            $table->foreign('message_id')->references('id')->on('message')->onDelete('no action')->onUpdate('no action');
            // Indexing
            $table->index(['id']);
            $table->index(['message_id']);
        });

        // Message Map (maps replies to messages)
        Schema::create('message_map', function (Blueprint $table) {
            // Fields
            $table->increments('id');
            $table->integer('parent_message_id')->unsigned();
            $table->integer('child_message_id')->unsigned();
            // Timestamps
            $table->timestamps();
            // FK
            $table->foreign('parent_message_id')->references('id')->on('message')->onDelete('no action')->onUpdate('no action');
            $table->foreign('child_message_id')->references('id')->on('message')->onDelete('no action')->onUpdate('no action');
            // Indexing
            $table->index(['id']);
            $table->index(['parent_message_id']);
        });

        // Guestbook Entry Map (maps messages to guestbooks)
        Schema::create('guestbook_entry_map', function (Blueprint $table) {
            // Fields
            $table->increments('id');
            $table->integer('guestbook_id')->unsigned();
            $table->integer('message_id')->unsigned();
            // Timestamps
            $table->timestamps();
            // FK
            $table->foreign('guestbook_id')->references('id')->on('guestbook')->onDelete('no action')->onUpdate('no action');
            $table->foreign('message_id')->references('id')->on('message')->onDelete('no action')->onUpdate('no action');
            // Indexing
            $table->index(['id']);
            $table->index(['guestbook_id']);
        });

        Schema::enableForeignKeyConstraints();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('guestbook_entry_map');
        Schema::dropIfExists('message_map');
        Schema::dropIfExists('message_history');
        Schema::dropIfExists('message');
        Schema::dropIfExists('guestbook');
    }
}
