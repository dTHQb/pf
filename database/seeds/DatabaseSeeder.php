<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $now = new \Carbon\Carbon();

        DB::table('users')->insert([
            'name' => 'admin',
            'email' => 'admin@pf.com',
            'password' => bcrypt('admin123'),
            'is_admin' => '1',
        ]);

        DB::table('guestbook')->insert([
            'created_by_id' => '1',
            'name' => 'Sample Book',
            'description' => 'This book is for demo purposes. This is only used to show the concept and will be the only book available',
            'created_at' => $now->format('Y-m-d H:i:s'),
        ]);
    }
}
