@extends('layouts.app')

@php
    $user = auth()->user();

    $isAdmin = $user->is_admin;
    $userId = $user->id;
@endphp

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            @if(count($guestbooks))
                @foreach ($guestbooks as $guestbook)
                    <div class="card">
                        <div class="card-header">
                            <a href="javascript:void(0)" onclick="toggleBookDescription({{ $guestbook->id }})">
                                {{ $guestbook->name }}
                            </a>
                        </div>

                        <div class="card-body">
                            <div class="book-description" id="{{ $guestbook->id }}" style="display: none">
                                {{ $guestbook->description }}
                            </div>

                            @if(!$isAdmin)
                                <div>
                                    <a href="javascript:void(0)" onclick="makeNewComment({{ $guestbook->id }})">{{ __('New Comment') }}</a>
                                </div>

                                <div class="book-new-comment" id="comment-{{ $guestbook->id }}"></div>
                            @endif

                            @php
                                $guestbookEntries = $guestbook->guestbook_entries;
                                $sampleSize = 5;
                                $size = 0;
                            @endphp

                            @if (count($guestbookEntries))
                                @foreach($guestbookEntries as $guestbookEntry)
                                    @if($size >= $sampleSize)
                                        @continue
                                    @endif

                                    @php
                                        $isUserCreator = $userId == $guestbookEntry->message->created_by_id;
                                        $isRemoved = $guestbookEntry->message->deleted_at !== null || $guestbookEntry->message->deleted_by_id !== null;
                                    @endphp

                                    @if(!$isAdmin && $isRemoved)
                                        @continue
                                    @endif

                                    <div class="alert">
                                        {{ $guestbookEntry->message->created_by->name }}
                                        {{ __(' said:') }}

                                        @if(!$isRemoved)
                                            <span>{{ $guestbookEntry->message->description }}</span>

                                            <span>
                                                @if($isAdmin)
                                                    <a href="javascript:void(0)" onclick="replyOnComment({{ $guestbookEntry->message->id }})">Reply</a>
                                                    <div class="book-comment" id="reply-{{ $guestbookEntry->message->id }}"></div>
                                                @endif

                                                @if($isUserCreator)
                                                    <a href="javascript:void(0)" onclick="updateComment({{ $guestbookEntry->message->id }})">Update</a>
                                                    <div class="book-comment" id="update-{{ $guestbookEntry->message->id }}"></div>
                                                @endif

                                                @if($isAdmin || $isUserCreator)
                                                    <a href="javascript:void(0)" onclick="deleteComment({{ $guestbookEntry->message->id }})">Remove</a>
                                                    <div class="book-comment" id="delete-{{ $guestbookEntry->message->id }}"></div>
                                                @endif
                                            </span>
                                        @else
                                            <span style="color: red"><s>{{ $guestbookEntry->message->description }}</s></span>
                                        @endif

                                        @php($replyCount = count($guestbookEntry->message->message_replies))

                                        @if($replyCount)
                                            <a href="javascript:void(0)" onclick="loadReplies({{ $guestbookEntry->message->id }})">{{ __('View Replies (' . $replyCount . ')') }}</a>

                                            <div class="replies" id="message-{{ $guestbookEntry->message->id }}"></div>
                                        @endif
                                    </div>

                                    @php($size += 1)
                                @endforeach
                            @else
                                <div class="alert">
                                    {{ __('No comments made yet') }}
                                </div>
                            @endif
                        </div>
                    </div>
                @endforeach
            @else
                <div class="card">
                    <div class="card-header">{{ __('Setup not done correctly') }}</div>

                    <div class="card-body">
                        <div class="alert alert-error" role="alert">
                            {{ __('Have a look at the Readme.md file for full instructions') }}
                        </div>
                    </div>
                </div>
            @endif
        </div>
    </div>
</div>
@endsection

<script type="text/javascript">
    function toggleBookDescription(id) {
        $('#' + id + '.book-description').toggle();
    }

    function loadReplies(id) {
        var repliesDiv = $('#message-' + id + '.replies');

        $.ajax({
            url: "{{ route('load_replies') }}",
            data: {message_id: id},
            success: function (result) {
                repliesDiv.html(result);
            }
        });
    }

    function makeNewComment(id) {
        var newCommentDiv = $('#comment-' + id + '.book-new-comment');

        $.ajax({
            url: "{{ route('load_comment_form') }}",
            data: {guestbook_id: id},
            success: function (result) {
                newCommentDiv.html(result);
            }
        });
    }

    function deleteComment(id) {
        var deleteCommentDiv = $('#delete-' + id + '.book-comment');

        $.ajax({
            url: "{{ route('load_delete_comment_form') }}",
            data: {message_id: id},
            success: function (result) {
                deleteCommentDiv.html(result);
            }
        });
    }

    function replyOnComment(id) {
        var replyCommentDiv = $('#reply-' + id + '.book-comment');

        $.ajax({
            url: "{{ route('load_reply_form') }}",
            data: {message_id: id},
            success: function (result) {
                replyCommentDiv.html(result);
            }
        });
    }

    function updateComment(id) {
        var updateCommentDiv = $('#update-' + id + '.book-comment');

        $.ajax({
            url: "{{ route('load_update_form') }}",
            data: {message_id: id},
            success: function (result) {
                updateCommentDiv.html(result);
            }
        });
    }
</script>
