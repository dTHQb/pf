@php
    $user = auth()->user();

    $isAdmin = $user->is_admin;
@endphp

@foreach($replies as $reply)
    @php
        $isRemoved = $reply->child_message->deleted_at !== null || $reply->child_message->deleted_by_id !== null;
    @endphp

    @if(!$isAdmin && $isRemoved)
        @continue
    @endif

    <div class="alert">
        {{ $reply->child_message->created_by->name }}
        {{ __(' replied:') }}

        @if(!$isRemoved)
            <span>{{ $reply->child_message->description }}</span>

            <span>
                @if($isAdmin)
                    <a href="javascript:void(0)" onclick="deleteComment({{ $reply->child_message->id }})">Remove</a>
                    <div class="book-comment" id="delete-reply-{{ $reply->child_message->id }}"></div>
                @endif
            </span>
        @else
            <span style="color: red"><s>{{ $reply->child_message->description }}</s></span>
        @endif
    </div>
@endforeach

<script type="text/javascript">
    function deleteComment(id) {
        var deleteCommentDiv = $('#delete-reply-' + id + '.book-comment');

        $.ajax({
            url: "{{ route('load_delete_comment_form') }}",
            data: {message_id: id},
            success: function (result) {
                deleteCommentDiv.html(result);
            }
        });
    }
</script>
