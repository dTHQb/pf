<form method="post" action="{{ route('create_reply') }}">
    <div class="form-group">
        @csrf

        <label for="description">{{ __('Reply :') }}</label>

        <input type="text" class="form-control" name="description"/>
    </div>

    <div class="form-group">
        <input type="hidden" class="form-control" name="message_id" value="{{ $messageId }}"/>
    </div>

    <button type="submit" class="btn btn-primary">{{ __('Create Reply') }}</button>
</form>