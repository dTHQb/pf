<form method="post" action="{{ route('delete_comment') }}">
    <div class="form-group">
        @csrf

        <label for="description">{{ __('Are you sure?') }}</label>
    </div>

    <div class="form-group">
        <input type="hidden" class="form-control" name="message_id" value="{{ $messageId }}"/>
    </div>

    <button type="submit" class="btn btn-danger">{{ __('Yes Delete') }}</button>
</form>