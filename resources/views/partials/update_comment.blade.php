@if($message !== null)
    <form method="post" action="{{ route('update_comment') }}">
        <div class="form-group">
            @csrf

            <label for="description">{{ __('Comment :') }}</label>

            <input type="text" class="form-control" name="description" value="{{ $message->description }}"/>
        </div>

        <div class="form-group">
            <input type="hidden" class="form-control" name="message_id" value="{{ $message->id }}"/>
        </div>

        <button type="submit" class="btn btn-primary">{{ __('Update Comment') }}</button>
    </form>
@else
    <div class="alert">
        {{ __('Message deleted') }}
    </div>
@endif
