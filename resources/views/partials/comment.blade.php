<form method="post" action="{{ route('create_comment') }}">
    <div class="form-group">
        @csrf

        <label for="description">{{ __('Comment :') }}</label>

        <input type="text" class="form-control" name="description"/>
    </div>

    <div class="form-group">
        <input type="hidden" class="form-control" name="guestbook_id" value="{{ $guestbookId }}"/>
    </div>

    <button type="submit" class="btn btn-primary">{{ __('Create Comment') }}</button>
</form>