# PF Assignment

How to setup:
1. Use GIT to clone the project
1. Open terminal inside project root
1. Run `composer up`
1. Run `npm install`
1. Run `npm run prod`
1. Run `cp .env.example .env`
1. Run `nano .env` set ready for production and update DB details
1. Create a database called *payfast*
1. Run `php artisan migrate`
1. Run `php artisan db:seed`
1. Follow general apache/nginx setup for vhost