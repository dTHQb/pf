<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/message/replies', 'MessageController@loadReplies')->name('load_replies');

Route::get('/message/loadCommentForm', 'MessageController@loadCommentForm')->name('load_comment_form');
Route::get('/message/loadDeleteCommentForm', 'MessageController@loadDeleteCommentForm')->name('load_delete_comment_form');
Route::get('/message/loadReplyForm', 'MessageController@loadReplyForm')->name('load_reply_form');
Route::get('/message/loadUpdateForm', 'MessageController@loadUpdateForm')->name('load_update_form');
Route::post('/message/createComment', 'MessageController@createComment')->name('create_comment');
Route::post('/message/deleteComment', 'MessageController@deleteComment')->name('delete_comment');
Route::post('/message/createReply', 'MessageController@createReply')->name('create_reply');
Route::post('/message/updateComment', 'MessageController@updateComment')->name('update_comment');
