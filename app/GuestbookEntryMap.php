<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GuestbookEntryMap extends Model
{
    protected $table = 'guestbook_entry_map';

    protected $fillable = [
        'guestbook_id',
        'message_id',
    ];

    protected $dates = [
        'created_at',
        'updated_at',
    ];

    public function guestbook()
    {
        return $this->belongsTo(Guestbook::class, 'guestbook_id', 'id');
    }

    public function message()
    {
        return $this->hasOne(Message::class, 'id', 'message_id')->withTrashed();
    }
}
