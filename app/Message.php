<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Message extends Model
{
    use SoftDeletes;

    protected $table = 'message';

    protected $fillable = [
        'created_by_id',
        'deleted_by_id',
        'description',
    ];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    public function created_by()
    {
        return $this->hasOne(User::class, 'id', 'created_by_id');
    }

    public function deleted_by()
    {
        return $this->hasOne(User::class, 'id', 'deleted_by_id');
    }

    public function message_replies()
    {
        $message_replies = $this->hasMany(MessageMap::class, 'parent_message_id');

        if (auth()->user() !== null && !auth()->user()->is_admin) {
            $message_replies->join('message', 'child_message_id', 'message.id')->where('message.deleted_at', 'is not', 'null');
        }

        return $message_replies;

    }

    public function history()
    {
        return $this->hasMany(MessageHistory::class);
    }
}
