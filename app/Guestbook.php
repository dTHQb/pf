<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Guestbook extends Model
{
    use SoftDeletes;

    protected $table = 'guestbook';

    protected $fillable = [
        'created_by_id',
        'deleted_by_id',
        'name',
        'description',
    ];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    public function created_by()
    {
        return $this->hasOne(User::class, 'id', 'created_by_id');
    }

    public function deleted_by()
    {
        return $this->hasOne(User::class, 'id', 'deleted_by_id');
    }

    public function guestbook_entries()
    {
        return $this->hasMany(GuestbookEntryMap::class)->orderBy('id', 'desc');
    }
}
