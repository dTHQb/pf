<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MessageMap extends Model
{
    protected $table = 'message_map';

    protected $fillable = [
        'parent_message_id',
        'child_message_id',
    ];

    protected $dates = [
        'created_at',
        'updated_at',
    ];

    public function parent_message()
    {
        return $this->belongsTo(Message::class, 'parent_message_id', 'id');
    }

    public function child_message()
    {
        return $this->hasOne(Message::class, 'id', 'child_message_id')->withTrashed();
    }
}
