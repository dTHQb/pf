<?php

namespace App\Http\Controllers;

use App\GuestbookEntryMap;
use App\Message;
use App\MessageHistory;
use App\MessageMap;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MessageController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Loads the replies for a message
     *
     * @return String
     */
    public function loadReplies(Request $request)
    {
        $replies = MessageMap::where(['parent_message_id' => $request->message_id])->get();

        return (String) view('partials.replies', compact('replies'));
    }

    public function loadCommentForm(Request $request)
    {
        $guestbookId = $request->guestbook_id;

        return (String) view('partials.comment', compact('guestbookId'));
    }

    public function loadReplyForm(Request $request)
    {
        $messageId = $request->message_id;

        return (String) view('partials.reply', compact('messageId'));
    }

    public function loadDeleteCommentForm(Request $request)
    {
        $messageId = $request->message_id;

        return (String) view('partials.delete_comment', compact('messageId'));
    }

    public function loadUpdateForm(Request $request)
    {
        $messageId = $request->message_id;
        $message = Message::find($messageId);

        return (String) view('partials.update_comment', compact('message'));
    }

    public function createComment(Request $request)
    {
        $user = auth()->user();

        try {
            $dbMessageTransaction = DB::transaction(function () use ($request, $user) {
                $message = Message::create(array(
                    'created_by_id' => $user->id,
                    'description' => $request->description,
                ));

                $guestbookEntryMap = GuestbookEntryMap::create(array(
                    'guestbook_id' => $request->guestbook_id,
                    'message_id' => $message->id,
                ));
            });
        }
        catch (\Exception $exception) {
            // SET FLASH FOR FAILURE
            flash('Creating comment failed')->error();
        }

        // SET FLASH FOR SUCCESS
        flash('Created comment successfully')->success();

        return redirect('home');
    }

    public function deleteComment(Request $request)
    {
        $user = auth()->user();
        $message = Message::find($request->message_id);

        $message->deleted_by_id = $user->id;
        $message->save();

        $message->delete();

        // SET FLASH FOR SUCCESS
        flash('Deleted comment successfully')->warning();

        return redirect('home');
    }

    public function createReply(Request $request)
    {
        $user = auth()->user();

        try {
            $dbMessageTransaction = DB::transaction(function () use ($request, $user) {
                $message = Message::create(array(
                    'created_by_id' => $user->id,
                    'description' => $request->description,
                ));

                $messageMap = MessageMap::create(array(
                    'parent_message_id' => $request->message_id,
                    'child_message_id' => $message->id,
                ));
            });
        }
        catch (\Exception $exception) { throw $exception;
            // SET FLASH FOR FAILURE
            flash('Creating reply failed')->error();
        }

        // SET FLASH FOR SUCCESS
        flash('Created reply successfully')->success();

        return redirect('home');
    }

    public function updateComment(Request $request)
    {
        $user = auth()->user();

        try {
            $message = Message::find($request->message_id);

            $dbMessageTransaction = DB::transaction(function () use ($request, $message) {
                $messageHistory = MessageHistory::create(array(
                    'message_id' => $request->message_id,
                    'description' => $message->description,
                ));
            });

            $message->description = $request->description;

            $message->save();

        }
        catch (\Exception $exception) {
            // SET FLASH FOR FAILURE
            flash('Updating comment failed')->error();
        }

        // SET FLASH FOR SUCCESS
        flash('Updated comment successfully')->success();

        // FIRE EVENT TO NOTIFY ADMIN IF THEY ALREADY REPLIED THAT THE ORIGINAL WAS UPDATED (Future Development)

        return redirect('home');
    }
}
